import { CheckAns } from "./CheckAns";

namespace LeetCode0995MinKBitFlips {

    function minKBitFlips(A: number[], K: number): number {
        if (!A || A.length == 0) return 0;
        // fixed the value of K
        if (K <= 0) K = 1;
        // left to right
        let ans = 0;
        const len = A.length;
        for (let i = 0; i < len; i++) {
            // find first zero
            if (A[i]) continue;
            // not enough position for K
            if (i + K > len) return -1;
            // changing
            for (let j = 0; j < K; j++) {
                A[j + i] = A[j + i] ^ 1;
            }
            ans++;
        }
        return ans;
    };

    // test
    function main() {
        const testData: { A: number[], k: number, ans: number }[] = [
            { A: [0, 1, 0], k: 1, ans: 2 },
            { A: [1, 1, 0], k: 2, ans: -1 },
            { A: [0, 0, 0, 1, 0, 1, 1, 0], k: 3, ans: 3 }
        ];
        for (const data of testData) {
            const ans = minKBitFlips(data.A, data.k);
            CheckAns(data, ans);
        }
    }
    main();

}