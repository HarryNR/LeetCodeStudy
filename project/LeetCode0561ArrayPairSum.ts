import { CheckAns } from "./CheckAns";

namespace LeetCode0561ArrayPairSum {

    function arrayPairSum(nums: number[]): number {
        if (!nums) return 0;
        nums.sort((a: number, b: number) => a - b);
        let ans = 0;
        for (let i = 0; i < nums.length; i += 2) {
            ans += nums[i];
        }
        return ans;
    };

    // test
    const testData: { nums: number[], ans: number }[] = [
        { nums: [1, 4, 3, 2], ans: 4 },
        { nums: [6, 2, 6, 5, 1, 2], ans: 9 }
    ];
    for (const data of testData) {
        const ans = arrayPairSum(data.nums);
        CheckAns(data, ans);
    }
}