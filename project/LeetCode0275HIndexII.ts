namespace LeetCode0275HIndexII {

    function hIndex(citations: number[]): number {
        if (!citations || citations.length == 0)
            return 0;
        const size: number = citations.length;
        // 已序队列，折半查找
        let l0 = 0, l1 = size - 1;
        while (l0 <= l1) {
            const mid = l0 + Math.floor((l1 - l0) / 2);
            if (citations[mid] >= size - mid) {
                // 指数H (citations[mid]) >= 符合要求的论文数count （size-mid）; 符合要求，修改右边界继续测试；
                l1 = mid - 1;
            } else {
                // 指数H (citations[mid]) < 符合要求的论文数count （size-mid）; 不符合要求，修改左边界继续测试；
                l0 = mid + 1;
            }
        }
        return size - l0;
    };

}