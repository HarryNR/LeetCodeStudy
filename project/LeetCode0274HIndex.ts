namespace LeetCode0274HIndex {

    function hIndex(citations: number[]): number {
        let h = 0;
        if (!citations || citations.length == 0)
            return h;
        // sort 倒叙排列
        citations.sort((a, b) => b - a);
        // 遍历
        let size = citations.length;
        while (h < size && citations[h] > h) {
            h++;
        }
        return h;
    };

}