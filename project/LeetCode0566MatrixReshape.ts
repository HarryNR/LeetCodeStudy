
namespace LeetCode0566MatrixReshape {

    function matrixReshape(nums: number[][], r: number, c: number): number[][] {
        if (!nums || !nums[0])
            return nums;
        const co = nums[0].length;
        const len = nums.length * co;
        if (r * c != len)
            return nums;
        let ary: number[][] = new Array(r).fill(0).map(() => new Array(c).fill(0));
        for (let i = 0; i < len; ++i) {
            ary[Math.floor(i / c)][i % c] = nums[Math.floor(i / co)][i % co];
        }
        return ary;
    };

}