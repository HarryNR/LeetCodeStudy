import { CheckAns } from "./CheckAns";

namespace LeetCode1208EqualSubstring {

    // 双指针直接计算
    function equalSubstring(s: string, t: string, maxCost: number): number {
        // 题目条件判断
        if (!s || !t || maxCost == NaN || s.length != t.length) return 0;
        let left = 0;
        let right = 0;
        let curCost = 0;
        while (right < s.length) {
            const charCost = Math.abs(s.charCodeAt(right) - t.charCodeAt(right));
            curCost += charCost;
            if (curCost > maxCost) {
                curCost -= Math.abs(s.charCodeAt(left) - t.charCodeAt(left));
                left++;
            }
            right++;
        }
        return right - left;
    };

    // test
    const testData: { s: string, t: string, cost: number, ans: number }[] = [
        { s: "abcd", t: "bcdf", cost: 3, ans: 3 },
        { s: "abcd", t: "cdef", cost: 3, ans: 1 },
        { s: "abcd", t: "acde", cost: 0, ans: 1 }
    ];
    for (const data of testData) {
        const ans = equalSubstring(data.s, data.t, data.cost);
        CheckAns(data, ans);
    }
}