import { CheckAns } from "./CheckAns";

namespace MSJDSix0101UniqueChar {

    function isUnique(astr: string): boolean {
        if (!astr || astr.length <= 1)
            return true;
        // 利用string函数
        for (const char of astr) {
            if (astr.indexOf(char) != astr.lastIndexOf(char))
                return false;
        }
        return true;
    };


    function isUnique1(astr: string): boolean {
        if (!astr || astr.length <= 1)
            return true;
        // 利用hash表键值对特性
        let map: Map<string, number> = new Map();
        for (let i = 0; i < astr.length; i++) {
            const char = astr[i];
            if (map.has(char)) {
                return false;
            }
            map.set(char, i);
        }
        return true;
    };

    function isUnique2(astr: string): boolean {
        if (!astr || astr.length <= 1)
            return true;
        // 利用set去除重复的特性
        const set: Set<string> = new Set(astr);
        return set.size == astr.length;
    };

    // test
    const testData: { s: string, ans: boolean }[] = [
        { s: "leetcode", ans: false },
        { s: "abc", ans: true },
        { s: "'\t\n\c asdr", ans: true },
        { s: "\t\n\c asdr啊 ", ans: false }
    ];
    for (const data of testData) {
        const ans = isUnique2(data.s);
        CheckAns(data, ans);
    }

}