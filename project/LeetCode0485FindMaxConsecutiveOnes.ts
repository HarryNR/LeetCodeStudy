import { CheckAns } from "./CheckAns";

namespace LeetCode0485FindMaxConsecutiveOnes {

    function findMaxConsecutiveOnes(nums: number[]): number {
        if (!nums || nums.length == 0) return 0;
        let ans = 0;
        let temp = 0;
        nums.forEach((val: number) => {
            if (val == 1) {
                temp++;
                ans = Math.max(ans, temp);
            }
            else {
                temp = 0;
            }
        });
        // for (const val of nums) {
        //     if (val == 1) {
        //         temp++;
        //         ans = Math.max(ans, temp);
        //     }
        //     else {
        //         temp = 0;
        //     }
        // }
        return ans;
    };

    // test
    const testData: { nums: number[], ans: number }[] = [
        { nums: [1, 1, 0, 1, 1, 1], ans: 3 },
        { nums: [1], ans: 1 }
    ];
    for (const data of testData) {
        const ans = findMaxConsecutiveOnes(data.nums);
        CheckAns(data, ans);
    }
}