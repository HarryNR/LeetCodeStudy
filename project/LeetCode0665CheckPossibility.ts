import { CheckAns } from "./CheckAns";

namespace LeetCode0665CheckPossibility {

    // 直接按照定义来，需要统计修改次数。超过1次，即可终止遍历，返回false；
    // 修改问题：改大还是改小问题，即方向性问题；从i=0位置顺序向右侧遍历，则取i-1的位置与i+1也比较，防止破坏左侧已检测数列，决定改小ary[i]或者改大ary[i+1];
    function checkPossibility(nums: number[]): boolean {
        if (!nums || nums.length <= 1) return true;
        let hasFixed: boolean = false;
        const posEnd: number = nums.length - 1;    //  右侧遍历边界
        for (let i = 0; i < posEnd; i++) {
            if (nums[i] > nums[i + 1]) {
                if (hasFixed)
                    return false;
                // fixed number
                if (i != 0 && nums[i - 1] <= nums[i + 1] || i == 0) {
                    // nums[i - 1] <= nums[i + 1]，或者i==0，则改小ary[i]
                    nums[i] = nums[i + 1];
                }
                else {
                    nums[i + 1] = nums[i];
                }
                hasFixed = true;
            }
        }
        return true;
    };

    // test
    const testData: { nums: number[], ans: boolean }[] = [
        { nums: [3, 4, 2, 3], ans: false },
        { nums: [4, 2, 3], ans: true },
        { nums: [4, 2, 1], ans: false },
        { nums: [-1, 4, 2, 3], ans: true },
        { nums: [2, 3, 3, 2, 4], ans: true },
        { nums: [1], ans: true }
    ];
    for (const data of testData) {
        const ans = checkPossibility(data.nums);
        CheckAns(data, ans);
    }
}