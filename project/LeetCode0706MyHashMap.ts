
namespace LeetCode0706MyHashMap {
    class MyHashMap {
        private BASE: number = 0;
        private data: number[][][] = [];
    
        constructor() {
            this.BASE = 769;
            this.data = new Array(this.BASE).fill(0).map(() => new Array());
        }
    
        put(key: number, value: number): void {
            const h: number = this.hash(key);
            for (const it of this.data[h]) {
                if (it[0] === key) {
                    it[1] = value;
                    return;
                }
            }
            this.data[h].push([key, value]);
        }
    
        get(key: number): number {
            const h: number = this.hash(key);
            for (const it of this.data[h]) {
                if (it[0] === key) {
                    return it[1];
                }
            }
            return -1;
        }
    
        remove(key: number): void {
            const h:number = this.hash(key);
            for (const it of this.data[h]) {
                if (it[0] === key) {
                    const idx = this.data[h].indexOf(it);
                    this.data[h].splice(idx, 1);
                    return;
                }
            }
        }
    
        public hash(key:number) {
            return key % this.BASE;
        }
    }
    
    /**
     * Your MyHashMap object will be instantiated and called as such:
     * var obj = new MyHashMap()
     * obj.put(key,value)
     * var param_2 = obj.get(key)
     * obj.remove(key)
     */
}