
namespace AlgorithmSelectionSort {
    // show debug log
    const isDebug: boolean = true;

    // 选择排序(默认升序排序)
    function selectionSort(arr: number[]) {
        const len: number = arr.length;
        if (len <= 1) return;
        if (isDebug)
            console.log(`数组长度${len}，排序前数组：\n`, JSON.stringify(arr));
        for (let i = 0; i < len - 1; i++) {
            let posMin: number = i;
            for (let j = i + 1; j < len; j++) {
                if (arr[j] < arr[posMin]) {     // 寻找最小的数
                    posMin = j;                 // 将最小数的索引保存
                }
            }
            if (posMin != i) {
                const temp: number = arr[i];
                arr[i] = arr[posMin];
                arr[posMin] = temp;
            }
            if (isDebug)
                console.log(`从数组位置${i}开始搜索，最小元素位置为${posMin}，排序后数组：\n`, JSON.stringify(arr));
        }
        return arr;
    }


    function main() {
        const ary: number[] = [22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70];
        selectionSort(ary);
    }
    main();
}
