namespace LeetCode1736MaxNumTime {

    function maximumTime(time: string): string {
        // check error input
        if (!time || time.length != 5)
            return '';
        /// 按照文本格式处理 a[0] => [0-2] a[1] => {a[0]<2? [0-9]:[0-4]} a[3,4] => [0-9]
        let ary: string[] = time.split('');
        if (ary[0] === '?')
            ary[0] = ('4' <= ary[1] && ary[1] <= '9') ? '1' : '2';
        if (ary[1] === '?')
            ary[1] = (ary[0] == '2') ? '3' : '9';
        if (ary[3] === '?')
            ary[3] = '5';
        if (ary[4] === '?')
            ary[4] = '9';
        return ary.join('');
    };
}