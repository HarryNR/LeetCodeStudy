namespace Face1002GroupAnagrams {
    /**
     * 编写一种方法，对字符串数组进行排序，将所有变位词组合在一起。变位词是指字母相同，但排列不同的字符串。
     * 说明：
        所有输入均为小写字母。
        不考虑答案输出的顺序。
     * 示例:
        输入: ["eat", "tea", "tan", "ate", "nat", "bat"],
        输出: [ ["ate","eat","tea"], ["nat","tan"], ["bat"] ]
     */
    function groupAnagrams(strs: string[]): string[][] {
        // check array
        if (!strs || strs.length == 0)
            return [];
        // search and deal data
        let list: GroupRecords[] = new Array();
        strs.forEach((v: string) => {
            // 无效字符串、空字符串不处理 (更正：测试用例有空字符串，所以更改处理方式)
            // if (!v || v.length == 0)
            //     return;
            // 这里加个判断，长度为1的字符串，不需要排序处理
            const sortedStr: string = (!v || v.length) <= 1 ? v
                : v.split('').sort((a, b) => a.charCodeAt(0) - b.charCodeAt(0)).join();
            const strLen: number = !v ? 0 : sortedStr.length;
            // 遍历 + 分组
            let hasFound: boolean = false;
            for (const data of list) {
                // quickly compare: check string length
                if (data.strLen != strLen)
                    continue;
                // 详细比较，查看字符是否全部符合要求
                if (data.sortedStr == sortedStr) {
                    hasFound = true;
                    if (!data.ary)
                        data.ary = new Array();
                    data.ary.push(v);
                    break;
                }
            }
            if (!hasFound)
                list.push({
                    strLen,
                    sortedStr,
                    ary: [v]
                });
        });
        // 合并数据
        let ans: string[][] = new Array();
        list.forEach((v: GroupRecords) => {
            ans.push(v.ary);
        });
        return ans;
    };

    interface GroupRecords {
        strLen: number,      // 字符串长度
        sortedStr: string,   // 已序的字符串
        ary: string[]        // 符合要求的字符串数组
    }

}