﻿#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class Solution {
public:
    int arrayPairSum(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int ans = 0;
        for (size_t i = 0; i < nums.size(); i += 2) {
            ans += nums[i];
        }
        return ans;
    }
};

int main() {
	vector<int> arr = { 1, 4, 3, 2 };
    cout << "want: 4, ans = " << Solution().arrayPairSum(arr) << endl;

    vector<int> arr1 = { 6, 2, 6, 5, 1, 2 };
    cout << "want: 9, ans = " << Solution().arrayPairSum(arr1) << endl;

	return 0;
}