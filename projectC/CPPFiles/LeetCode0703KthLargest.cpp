﻿#include <vector>
#include <iostream>
#include <queue>
#include <string>
#include <sstream>
using namespace std;


class KthLargest {
public:
	priority_queue<int, vector<int>, greater<int>> q;
	int k;
	KthLargest(int k, vector<int>& nums) {
		this->k = k;
		for (auto& x : nums) {
			add(x);
		}
	}

	int add(int val) {
		q.push(val);
		if (q.size() > k) {
			q.pop();
		}
		return q.top();
	}
};

// 测试用例
// 移除字符串左侧空字符,以及数组符号'['or'{' and ','
void trimLeftTrailingSpaces(string& input) {
	input.erase(input.begin(), find_if(input.begin(), input.end(), [](int ch) {
		return !isspace(ch) && ch != '[' && ch != '{' && ch != ',';
		}));
}
// 移除字符串右侧空字符,以及数组符号']'or'}'and','
void trimRightTrailingSpaces(string& input) {
	input.erase(find_if(input.rbegin(), input.rend(), [](int ch) {
		return !isspace(ch) && ch != ']' && ch != '}' && ch != ',';
		}).base(), input.end());
}

// 字符串转数组，用','作为分隔标记
vector<int> stringToIntegerVector(string input) {
	vector<int> output;
	trimLeftTrailingSpaces(input);
	trimRightTrailingSpaces(input);
	stringstream ss;
	ss.str(input);
	string item;
	char delim = ',';
	while (getline(ss, item, delim)) {
		output.push_back(stoi(item));
	}
	return output;
}

int main() {
	cout << "开始录入初始化数据："<< endl;
	string line;
	getline(cin, line);
	int k = stoi(line);
	getline(cin, line);
	vector<int> nums = stringToIntegerVector(line);
	KthLargest* ret = new KthLargest(k, nums);
	cout << "开始录入运算数据：" << endl;
	while (getline(cin, line)) {
		int val = stoi(line);
		int out = ret->add(val);
		cout << "输出：" << out << endl;
	}
	return 0;
}