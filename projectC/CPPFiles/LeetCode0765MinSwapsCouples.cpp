﻿#include <vector>
using namespace std;


class Solution {
public:
    int minSwapsCouples(vector<int>& row) {
        // 检查题目是否异常
        const size_t len = row.size();
        if (len < 4 || len % 2 != 0)
            return 0;
        // 开始解题:最终需求获得len/2个（0+2x,1+2x）形式的奇偶对，且无需排序；直接分治处理。
        int cnt = 0;
        for (size_t i = 0; i < len; i += 2) {
            const int left = row[i];
            const int want = left ^ 1;    // 异或1可以直接获得目标
            if (want != row[i + 1]) {
                bool find = false;
                for (size_t right = i + 2; right < len; right++) {
                    if (want == row[right]) {
                        // swap
                        row[right] = row[i + 1];
                        row[i + 1] = want;
                        find = true;
                        cnt++;
                        break;
                    }
                }
                // 未达成配对，题目异常，求解无意义并退出
                if (!find)
                    return 0;
            }
        }
        return cnt;
    }
};