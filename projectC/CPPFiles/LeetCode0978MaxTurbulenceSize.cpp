#include <vector>
#include <iostream>
using namespace std;

class Solution_maxTurbulenceSize {
public:
	int maxTurbulenceSize(vector<int>& arr) {
		const int len = arr.size();
		if (len <= 1) return len;
		int pos = 1;  // 指针位置
		int maxLen = 1; // 最长符合条件数组长度
		int curLen = 1; // 当前子数组长度
		int lastDelta = 0;  // 上一次的差值
		while (pos < len) {
			const int delta = arr[pos] - arr[pos - 1];
			if (delta == 0) {
				curLen = 1;
			}
			else if (delta * lastDelta > 0) {
				curLen = 2;
			}
			else {
				curLen++;
				if (curLen > maxLen)
					maxLen = curLen;
			}
			lastDelta = delta > 0 ? 1 : (delta < 0 ? -1 : 0);
			pos++;
		}
		return maxLen;
	}
};

int main()
{
	Solution_maxTurbulenceSize so;
	vector<int> num = { 9, 4, 2, 10, 7, 8, 8, 1, 9 };
	cout << so.maxTurbulenceSize(num) << endl;

	vector<int> num1 = { 4, 8, 12, 16 };
	cout << so.maxTurbulenceSize(num1) << endl;

	vector<int> num2 = { 100 };
	cout << so.maxTurbulenceSize(num2) << endl;

	return 0;
}