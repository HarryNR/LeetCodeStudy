﻿#include <vector>
#include <iostream>
using namespace std;

class Solution_twoSum
{
public:
    vector<int> twoSum(vector<int>& nums, int target)
    {
        // 有效性检测
        if (&nums == NULL || nums.size() < 2)
            return {};
        // 直接计算
        const int aryLength = nums.size() - 1;
        for (int i = 0; i < aryLength; i++)
        {
            for (int j = i + 1; j <= aryLength; j++)
            {
                // 已获得一组有效解，直接结束
                if (nums[j] + nums[i] == target)
                {
                    return { i, j };
                }
            }
        }

        return {};
    }
};

#include <iterator>
template <typename T>
std::ostream& operator<<(std::ostream& os, std::vector<T> vec)
{
    os << "{";
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(os, ""));
    os << "}";
    return os;
}

int main()
{
   Solution_twoSum so;
   vector<int> nums = { 2, 7, 11, 15 };
   const int target = 9;
   cout << so.twoSum(nums, target) << endl;

   vector<int> num1 = { 3, 2, 4 };
   const int target1 = 6;
   cout << so.twoSum(num1, target1) << endl;

   vector<int> num2 = { 3, 3 };
   const int target2 = 6;
   cout << so.twoSum(num2, target2) << endl;

   vector<int> num3 = { -1, -2, -3, -4, -5 };
   const int target3 = -8;
   cout << so.twoSum(num3, target3) << endl;

   return 0;
}
