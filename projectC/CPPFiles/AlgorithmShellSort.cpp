﻿#include <stdio.h>
#include <stdbool.h>

// show debug log
static bool isDebug = true;

// 打印数组
void printArray(int* ary, int len) {
	if (len == 0) {
		printf("[]");
		return;
	}
	// 字符串
	for (int i = 0; i < len; i++)
	{
		// 用数组‘,’形式分隔元素
		printf(i == 0 ? "[" : ",");
		printf("%d", ary[i]);
	}
	printf("]\n");
}

void insertionSort(int arr[], int len)
{
	if (len <= 1 || !arr) return;
	if (isDebug) {
		printf("数组长度%d，排序前数组：\n", len);
		printArray(arr, len);
	}
	// 希尔排序，步长用最简单的长度减半至1
	for (int gap = len >> 1; gap > 0; gap >>= 1)
	{
		if (isDebug)
			printf("\n设置步长为%d开始遍历\n", gap);
		// 插入排序方法
		for (int i = gap; i < len; i++)
		{
			int tarPos = i - gap;		// 目标位置(从当前位置前一个开始，向前查找)
			const int val = arr[i]; // 当前数值(做替换准备)
			while (tarPos >= 0 && arr[tarPos] > val) {
				arr[tarPos + gap] = arr[tarPos];
				tarPos -= gap;
			}
			// 指针停止，指针的下一个位置就是正确的序列位置
			arr[tarPos + gap] = val;
			if (isDebug) {
				printf("从数组位置%d开始搜索，目标元素位置为%d，排序后数组：\n", i, tarPos + gap);
				printArray(arr, len);
			}
		}
	}
}

int main() {
	int arr[] = { 22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70 };
	const int len = (int)sizeof(arr) / sizeof(*arr);
	insertionSort(arr, len);
	return 0;
}
