﻿#include <vector>
#include <iostream>
#include <string>
using namespace std;

class Solution_getRow {
public:
	vector<int> getRow(int rowIndex) {
		if (rowIndex < 0) return {};

		// 中位pos(第2行开始才需要计算,从0开始索引)
		const size_t midPos = rowIndex >= 2 ? (int)((rowIndex - 1) / 2.0 + 0.5) : 0;
		vector<int> ans;
		for (size_t i = 0; i <= (size_t)rowIndex; i++) {
			// 先处理第0、1、rowIndex-1、rowIndex处的4个特值
			int val = (i == 0 || i == rowIndex) ? 1 : (((i == 1 || i == rowIndex - 1)) ? rowIndex : 0);
			// 处理中间值，前一般需要数学计算，后一半直接对称取值
			if (val == 0) {
				if (i <= midPos) {
					// 前一个数字的下标
					const size_t last = i - 1;
					val = ans[last] * (rowIndex - last) / i;
				}
				else {
					const size_t pos = 2 * midPos - i + (rowIndex % 2);
					val = ans[pos];
				}

			}
			ans.push_back(val);
		}
		return ans;
	}
};

/// 测试用例
// 数组转字符串
string arrayToString(const vector<int>& ary) {
	if (ary.size() == 0) return "[]";
	// 结果字符串
	string result;
	for (size_t i = 0, len = ary.size(); i < len; i++)
	{
		// 用数组‘,’形式分隔元素
		result += to_string(ary[i]) + ",";
	}
	// 返回字符串时，添加“[]”，并且移除最后一个','
	return "[" + result.substr(0, result.length() - 1) + "]";
}

int main() {
	string line;
	while (getline(cin, line))
	{
		int k = stoi(line);
		vector<int> ans = Solution_getRow().getRow(k);
		cout << arrayToString(ans) << endl;
	}
	return 0;
}