﻿#include <vector>
#include <iostream>
using namespace std;

class Solution {
public:
    int findMaxConsecutiveOnes(vector<int>& nums) {
		const int len = nums.size();
		if (len <= 0) return 0;
		int ans = 0;
		int temp = 0;
		for (int i = 0; i < len; i++) {
			if (nums[i] == 1) {
				temp++;
				if (temp > ans)
					ans = temp;
			}
			else {
				temp = 0;
			}
		}
		return ans;
    }
};

int main()
{
	Solution so;
	vector<int> nums = { 1, 1, 0, 1, 1, 1 };
	cout << "want: 3, ans = " << so.findMaxConsecutiveOnes(nums) << endl;

	vector<int> nums1 = { 1 };
	cout << "want: 1, ans = " << so.findMaxConsecutiveOnes(nums1) << endl;

	return 0;
}