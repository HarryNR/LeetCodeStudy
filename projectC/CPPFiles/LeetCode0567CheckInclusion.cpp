﻿#include <string>
#include <vector>
#include <iostream>
using namespace std;

class Solution_checkInclusion {
public:
	bool checkInclusion(string s1, string s2) {
		if (&s1 == nullptr || &s2 == nullptr || s1.size() > s2.size())
			return false;
		// 处理s1字符串，因为题目要求不需要顺序，仅统计字符个数；且全小写字符，顾size=26的数组即可
		vector<int> aryS1(26);
		// 处理s2字符串前s1.length长度，因为题设s2.length>=s1.length
		vector<int> aryS2(26);
		// 循环遍历s1.length，统计s1字符，顺便统计s2前s1.length字符串
		const int charACode = 'a';
		const int s1Len = s1.size();
		for (int i = 0; i < s1Len; i++) {
			const int code1 = s1[i] - charACode;
			aryS1[code1]++;
			const int code2 = s2[i] - charACode;
			aryS2[code2]++;
		}
		// 比较一下字符串是否相等(直接使用vector提供的元素的比较重载符==),题意仅需找到一处匹配即可
		if (aryS1 == aryS2)
			return true;
		// 后续遍历，用双指针对数组aryS2直接进行维护，节省统计开销
		int right = s1Len;
		while (right < s2.size()) {
			// fixed the aryS2
			aryS2[s2[right] - charACode]++;
			aryS2[s2[right - s1Len] - charACode]--;
			// 比较一下字符串是否相等，题意仅需找到一处匹配即可
			if (aryS1 == aryS2)
				return true;
			right++;
		}
		return false;
	}
};

int main()
{
	Solution_checkInclusion so;
	string s11 = "ab";
	string s12 = "eidbaooo";
	cout << "want: true, ans = " << so.checkInclusion(s11, s12) << endl;

	string s21 = "ab";
	string s22 = "eidboaoo";
	cout << "want: false, ans = " << so.checkInclusion(s21, s22) << endl;

	return 0;
}
