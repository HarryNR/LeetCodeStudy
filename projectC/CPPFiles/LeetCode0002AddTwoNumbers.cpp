﻿#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
using namespace std;

// Definition for singly-linked list.
struct ListNode {
	int val;
	ListNode* next;
	ListNode() : val(0), next(nullptr) {}
	ListNode(int x) : val(x), next(nullptr) {}
	ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution_addTwoNumbers {
public:
	// 非const指针，直接修改l1链表作为返回值，递归调用
	ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
		// 强制l1链表为主线
		if (l1 == nullptr)
			return new ListNode();

		// l1，l2？ sum
		const int vs = l1->val + (l2 ? l2->val : 0);
        // fix l1 value
		l1->val = vs % 10;
        // 处理加法进位
		if (vs >= 10) {
			if (l1->next != nullptr) {
				l1->next->val += 1;
			}
			else {
				l1->next = new ListNode(1);
			}
		}
		// 预处理下次调用
		if (l1->next == nullptr && l2 != nullptr && l2->next) {
			l1->next = l2->next;
			l2 = nullptr;
		}
		// 递归调用
		Solution_addTwoNumbers so;
		so.addTwoNumbers(l1->next, l2!=nullptr ? l2->next : nullptr);

		// 返回主线l1链表头
		return l1;
	}

    // 优化，c/c++不使用递归，直接使用指针；因为l1 l2非空
    ListNode* addTwoNumbers2(ListNode* l1, ListNode* l2) {
        ListNode* ans = l1;

        while (l1) {
            // sum
            const int vs = l1->val + (l2 ? l2->val : 0);
            // fix l1 value
            l1->val = vs % 10;
            // 处理加法进位
            if (vs >= 10) {
                if (l1->next) {
                    l1->next->val += 1;
                }
                else {
                    l1->next = new ListNode(1);
                }
            }
            // 预处理下次循环时l1终结的问题
            if (l1->next == nullptr && l2 && l2->next) {
                l1->next = l2->next;
                l2 = nullptr;
            }
            // 指针移动
            if (l1) {
                l1 = l1->next;
            }
            if (l2) {
                l2 = l2->next;
            }
        }
        return ans;
    }
};


/// 编写测试用例，方便调试使用
// 移除字符串左侧空字符,以及数组符号'['or'{' and ','
void trimLeftTrailingSpaces(string& input) {
    input.erase(input.begin(), find_if(input.begin(), input.end(), [](int ch) {
		return !isspace(ch) && ch != '[' && ch != '{' && ch != ',';
        }));
}
// 移除字符串右侧空字符,以及数组符号']'or'}'and','
void trimRightTrailingSpaces(string& input) {
    input.erase(find_if(input.rbegin(), input.rend(), [](int ch) {
		return !isspace(ch) && ch != ']' && ch != '}' && ch != ',';
        }).base(), input.end());
}

// 字符串转数组，用','作为分隔标记
vector<int> stringToIntegerVector(string input) {
    vector<int> output;
    trimLeftTrailingSpaces(input);
    trimRightTrailingSpaces(input);
    stringstream ss;
    ss.str(input);
    string item;
    char delim = ',';
    while (getline(ss, item, delim)) {
        output.push_back(stoi(item));
    }
    return output;
}

// 字符串转列表
ListNode* stringToListNode(string input) {
    // Generate list from the input
    vector<int> list = stringToIntegerVector(input);

    // Now convert that list into linked list
    ListNode* dummyRoot = new ListNode(0);
    ListNode* ptr = dummyRoot;
    for (int item : list) {
        ptr->next = new ListNode(item);
        ptr = ptr->next;
    }
    ptr = dummyRoot->next;
    delete dummyRoot;
    return ptr;
}

// 链表转数组字符串，打印输出使用
string listNodeToString(ListNode* node) {
    if (node == nullptr) return "[]";
    // 结果字符串
    string result;
    while (node) {
        // 用数组‘,’形式分隔元素
        result += to_string(node->val) + ", ";
        // 移动指针
        node = node->next;
    }
    // 返回字符串时，添加“[]”，并且移除最后一个','
    return "[" + result.substr(0, result.length() - 2) + "]";
}

int main() {
    string line;
    while (getline(cin, line)) {
        ListNode* l1 = stringToListNode(line);
        getline(cin, line);
        ListNode* l2 = stringToListNode(line);

        ListNode* ret = Solution_addTwoNumbers().addTwoNumbers2(l1, l2);

        string out = listNodeToString(ret);
        cout << out << endl;
    }
    return 0;
}
