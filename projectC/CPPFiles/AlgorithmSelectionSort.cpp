﻿#include <stdio.h>
#include <algorithm>
#include <vector>

// show debug log
static bool isDebug = true;


// 打印数组
template<typename T>
void printArray(std::vector<T>& ary) {
	const int len = ary.size();
	if (len == 0) {
		printf("[]");
		return;
	}
	// 字符串
	for (int i = 0; i < len; i++)
	{
		// 用数组‘,’形式分隔元素
		printf(i == 0 ? "[" : ",");
		printf("%d", ary[i]);
	}
	printf("]\n");
}

template<typename T>
void selectionSort(std::vector<T>& arr) {
	const int len = arr.size();
	if (len <= 1) return;
	if (isDebug) {
		printf("数组长度%d，排序前数组：\n", len);
		printArray(arr);
	}
	for (int i = 0; i < len - 1; i++) {
		int min = i;
		for (int j = i + 1; j < len; j++)
			if (arr[j] < arr[min])
				min = j;
		if (min != i)
			std::swap(arr[i], arr[min]);
		if (isDebug) {
			printf("从数组位置%d开始搜索，最小元素位置为%d，排序后数组：\n", i, min);
			printArray(arr);
		}
	}
}

int main() {
	std::vector<int> arr = { 61, 17, 29, 22, 34, 60, 72, 21, 50, 1, 62 };
	selectionSort(arr);
	return 0;
}