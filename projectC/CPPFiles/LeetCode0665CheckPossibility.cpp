#include <vector>
#include <iostream>
using namespace std;

class Solution_checkPossibility
{
public:
    bool checkPossibility(vector<int>& nums)
    {
        if (&nums == nullptr || nums.size() <= 1)
            return true;
        bool hasFixed = false;
        int posEnd = nums.size() - 1; //  右侧遍历边界
        for (int i = 0; i < posEnd; i++)
        {
            if (nums[i] > nums[i + 1])
            {
                if (hasFixed)
                    return false;
                // fixed number
                if (i != 0 && nums[i - 1] <= nums[i + 1] || i == 0)
                {
                    // nums[i - 1] <= nums[i + 1]，或者i==0，则改小ary[i]
                    nums[i] = nums[i + 1];
                }
                else
                {
                    nums[i + 1] = nums[i];
                }
                hasFixed = true;
            }
        }
        return true;
    }
};

int main()
{
   Solution_checkPossibility so;
   vector<int> num = { 3, 4, 2, 3 };
   cout << so.checkPossibility(num) << endl;

   vector<int> num1 = { -1, 4, 2, 3 };
   cout << so.checkPossibility(num1) << endl;

   return 0;
}