﻿#include <stdlib.h>
#include <stdio.h>

int cmpfunc(const void* a, const void* b)
{
	return (*(int*)a - *(int*)b);
}

int arrayPairSum(int* nums, int numsSize) {
	qsort(nums, numsSize, sizeof(int), cmpfunc);
	int ans = 0;
	for (int i = 0; i < numsSize; i += 2) {
		ans += nums[i];
	}
	return ans;
}

int main()
{
	int num[] = { 1, 4, 3, 2 };
	printf("\nwant:4, ans = %d", arrayPairSum(num, sizeof(num) / sizeof(int)));

	int num1[] = { 6, 2, 6, 5, 1, 2 };
	printf("\nwant:9, ans = %d", arrayPairSum(num1, sizeof(num1) / sizeof(int)));

	return 0;
}