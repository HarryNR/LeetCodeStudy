﻿#include <stdio.h>
#include <stdlib.h>

// 1. Two Sum
int* twoSum(int* nums, int numsSize, int target, int* returnSize) {
    *returnSize = 0;
    // 有效性检测
    if (nums == NULL || numsSize < 2)
        return NULL;
    // 直接计算
    const int aryLength = numsSize - 1;
    for (int i = 0; i < aryLength; i++)
    {
        for (int j = i + 1; j <= aryLength; j++)
        {
            // 已获得一组有效解，直接结束
            if (nums[j] + nums[i] == target)
            {
                int* ans = malloc(sizeof(int) * 2);
                ans[0] = i;
                ans[1] = j;
                *returnSize = 2;
                return ans;
            }
        }
    }

    return NULL;
}

int main()
{
   int* returnSize = malloc(sizeof(int));
   int nums[] = { 2, 7, 11, 15 };
   const int target = 9;
   int* ans = twoSum(nums, 4, target, returnSize);
   printf("size = %d : ", *returnSize);
   for (int i = 0; i < *returnSize; i++)
   {
       printf("%d", ans[i]);
   }

   int num1[] = { 3, 2, 4 };
   const int target1 = 6;
   printf("\nsize = %d : ", *returnSize);
   int* ans1 = twoSum(num1, 3, target1, returnSize);
   for (int i = 0; i < *returnSize; i++)
   {
       printf("%d", ans1[i]);
   }

   int num2[] = { 3, 3 };
   const int target2 = 6;
   printf("\nsize = %d : ", *returnSize);
   int* ans2 = twoSum(num2, 2, target2, returnSize);
   for (int i = 0; i < *returnSize; i++)
   {
       printf("%d", ans2[i]);
   }

   int num3[] = { -1, -2, -3, -4, -5 };
   const int target3 = -8;
   printf("\nsize = %d : ", *returnSize);
   int* ans3 = twoSum(num3, 5, target3, returnSize);
   for (int i = 0; i < *returnSize; i++)
   {
       printf("%d", ans3[i]);
   }

   return 0;
}
