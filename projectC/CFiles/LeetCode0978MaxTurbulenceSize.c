﻿#include <stdio.h>

int maxTurbulenceSize(int* arr, int arrSize) {
	if (!arr) return 0;
	if (arrSize <= 1) return arrSize;
	int pos = 1;  // 指针位置
	int maxLen = 1; // 最长符合条件数组长度
	int curLen = 1; // 当前子数组长度
	int lastDelta = 0;  // 上一次的差值
	while (pos < arrSize) {
		const int delta = arr[pos] - arr[pos - 1];
		if (delta == 0) {
			curLen = 1;
		}
		else if (delta * lastDelta > 0) {
			curLen = 2;
		}
		else {
			curLen++;
			if (curLen > maxLen)
				maxLen = curLen;
		}
		lastDelta = delta > 0 ? 1 : (delta < 0 ? -1 : 0);
		pos++;
	}
	return maxLen;
}

int main()
{
	int num[] = { 9, 4, 2, 10, 7, 8, 8, 1, 9 };
	printf("%d", maxTurbulenceSize(num, 9));

	int num1[] = { 4, 8, 12, 16 };
	printf("%d", maxTurbulenceSize(num1, 4));

	int num2[] = { 100 };
	printf("%d", maxTurbulenceSize(num2, 1));

	return 0;
}
