﻿#include <stdio.h>
#include <stdbool.h>

// show debug log
static bool isDebug = true;

// 打印数组
void printArray(int* ary, int len) {
	if (len == 0) {
		printf("[]");
		return;
	}
	// 字符串
	for (int i = 0; i < len; i++)
	{
		// 用数组‘,’形式分隔元素
		printf(i == 0 ? "[" : ",");
		printf("%d", ary[i]);
	}
	printf("]\n");
}

void bubbleSort(int arr[], int len) {
	if (len <= 1 || !arr) return;
	if (isDebug) {
		printf("\n数组长度%d，排序前数组：\n", len);
		printArray(arr, len);
	}
	for (int i = 0; i < len - 1; i++) {
		if (isDebug) {
			printf("\n第%d次遍历，遍历至第%d个元素。\n", i, len - 1 - i);
		}
		for (int j = 0; j < len - 1 - i; j++)
		{
			bool isSwap = false;
			if (arr[j] > arr[j + 1]) {
				const int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
				isSwap = true;
			}
			if (isDebug) {
				printf("第%d个元素排序，是否发生交换%s，当前数组：\n", j,(isSwap?"true":"false"));
				printArray(arr, len);
			}
		}
	}
}

int main() {
	int arr[] = { 22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70 };
	const int len = (int)sizeof(arr) / sizeof(*arr);
	bubbleSort(arr, len);
	return 0;
}