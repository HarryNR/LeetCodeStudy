﻿#include <stdbool.h>

int minSwapsCouples(int* row, int rowSize) {
    // 检查题目是否异常
    if (!row) return 0;
    if (rowSize < 4 || rowSize % 2 != 0)
        return 0;
    // 开始解题:最终需求获得len/2个（0+2x,1+2x）形式的奇偶对，且无需排序；直接分治处理。
    int cnt = 0;
    for (int i = 0; i < rowSize; i += 2) {
        const left = row[i];
        const want = left ^ 1;    // 异或1可以直接获得目标
        if (want != row[i + 1]) {
            bool find = false;
            for (int right = i + 2; right < rowSize; right++) {
                if (want == row[right]) {
                    // swap
                    row[right] = row[i + 1];
                    row[i + 1] = want;
                    find = true;
                    cnt++;
                    break;
                }
            }
            // 未达成配对，题目异常，求解无意义并退出
            if (!find)
                return 0;
        }
    }
    return cnt;
}