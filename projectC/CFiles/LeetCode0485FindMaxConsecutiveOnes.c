﻿#include <stdio.h>

int findMaxConsecutiveOnes(int* nums, int numsSize) {
	if (!nums || numsSize <= 0) return 0;
	int ans = 0;
	int temp = 0;
	for (int i = 0; i < numsSize; i++) {
		if (nums[i] == 1) {
			temp++;
			if (temp > ans)
				ans = temp;
		}
		else {
			temp = 0;
		}
	}
	return ans;
}

int main()
{
	int num[] = { 1, 1, 0, 1, 1, 1 };
	int numsSize = sizeof(num) / sizeof(int);
	printf("%d", findMaxConsecutiveOnes(num, numsSize));

	int num1[] = { 1 };
	int numsSize1 = sizeof(num1) / sizeof(int);
	printf("%d", findMaxConsecutiveOnes(num1, numsSize1));

	return 0;
}