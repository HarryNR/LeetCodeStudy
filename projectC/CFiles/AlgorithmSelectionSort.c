﻿#include <stdio.h>
#include <stdbool.h>

// show debug log
static bool isDebug = true;

// 打印数组
void printArray(int* ary, int len) {
	if (len == 0) {
		printf("[]");
		return;
	}
	// 字符串
	for (int i = 0; i < len; i++)
	{
		// 用数组‘,’形式分隔元素
		printf(i == 0 ? "[" : ",");
		printf("%d", ary[i]);
	}
	printf("]\n");
}

void selectionSort(int arr[], int len)
{
	if (len <= 1 || !arr) return;
	if (isDebug) {
		printf("数组长度%d，排序前数组：\n", len);
		printArray(arr, len);
	}
	for (int i = 0; i < len - 1; i++)
	{
		int min = i;
		for (int j = i + 1; j < len; j++)     //走訪未排序的元素
			if (arr[j] < arr[min])    //找到目前最小值
				min = j;    //紀錄最小值
		if (min != i) {
			const int temp = arr[i];
			arr[i] = arr[min];
			arr[min] = temp;
		}
		if (isDebug) {
			printf("从数组位置%d开始搜索，最小元素位置为%d，排序后数组：\n", i, min);
			printArray(arr, len);
		}
	}
}

int main() {
	int arr[] = { 22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70 };
	const int len = (int)sizeof(arr) / sizeof(*arr);
	selectionSort(arr, len);
	return 0;
}
