#include <stdbool.h>
#include <stdio.h>

bool checkPossibility(int* nums, int numsSize)
{
	if (!nums || numsSize <= 1)
		return true;
	bool hasFixed = false;
	int posEnd = numsSize - 1; //  右侧遍历边界
	for (int i = 0; i < posEnd; i++)
	{
		if (nums[i] > nums[i + 1])
		{
			if (hasFixed)
				return false;
			// fixed number
			if (i != 0 && nums[i - 1] <= nums[i + 1] || i == 0)
			{
				// nums[i - 1] <= nums[i + 1]，或者i==0，则改小ary[i]
				nums[i] = nums[i + 1];
			}
			else
			{
				nums[i + 1] = nums[i];
			}
			hasFixed = true;
		}
	}
	return true;
}

int main()
{
	int num[] = { 3, 4, 2, 3 };
	int numsSize = 4;
	printf("%d", checkPossibility(num, numsSize));

	int num1[] = { -1, 4, 2, 3 };
	int numsSize1 = 4;
	printf("%d", checkPossibility(num1, numsSize1));

	return 0;
}