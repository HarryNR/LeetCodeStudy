﻿#include <stdio.h>
#include <stdlib.h>

int* getRow(int rowIndex, int* returnSize) {
	if (rowIndex < 0) return NULL;
	*returnSize = rowIndex + 1;
	// 中位pos(第2行开始才需要计算,从0开始索引)
	const int midPos = rowIndex >= 2 ? (int)((rowIndex - 1) / 2.0 + 0.5) : 0;
	int* ans = malloc(sizeof(int) * *returnSize);
	for (int i = 0; i <= rowIndex; i++) {
		// 先处理第0、1、rowIndex-1、rowIndex处的4个特值
		int val = (i == 0 || i == rowIndex) ? 1 : (((i == 1 || i == rowIndex - 1)) ? rowIndex : 0);
		// 处理中间值，前一般需要数学计算，后一半直接对称取值
		if (val == 0) {
			if (i <= midPos) {
				// 前一个数字的下标
				const int last = i - 1;
				// 先处理除法，或者乘法时用double计算，否则会溢出int表示的范围；
				val = (int)(ans[last] * (double)(rowIndex - last) / i);
			}
			else {
				const int pos = 2 * midPos - i + (rowIndex % 2);
				val = ans[pos];
			}
		}
		ans[i] = val;
	}
	return ans;
}


/// 测试用例
// 打印数组
void arrayToString(int* ary, int len) {
	if (len == 0) {
		printf("[]");
		return;
	}
	// 字符串
	for (int i = 0; i < len; i++)
	{
		// 用数组‘,’形式分隔元素
		printf(i == 0 ? "[" : ",");
		printf("%d", ary[i]);
	}
	printf("]");
}

int main() {
	int k = 0;
	while (k >= 0)
	{
		scanf_s("%d", &k);
		int len = 0;
		int* ary = getRow(k, &len);
		arrayToString(ary, len);
		if (!ary)
			free(ary);
		ary = NULL;
	}

	return 0;
}
