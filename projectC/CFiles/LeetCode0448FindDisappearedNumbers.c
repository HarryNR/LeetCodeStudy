﻿#include <stdio.h>
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* findDisappearedNumbers(int* nums, int numsSize, int* returnSize) {
	*returnSize = 0;
	if (numsSize <= 0)
		return NULL;
	// 还是老配方、老味道
	for (int i = 0; i < numsSize; i++) {
		int x = (nums[i] - 1) % numsSize;
		nums[x] += numsSize;
	}
	// 不知道要开辟多大的数组，按照最坏情况，先申请一块(numsSize - 1)的int字节数的内存；（最坏的情况就是nums数组全是相同的数）
	int* ans = malloc(sizeof(int) * (numsSize - 1));
	for (int i = 0; i < numsSize; i++) {
		if (nums[i] <= numsSize) {
			ans[*returnSize] = i + 1;
			(*returnSize)++;
		}
	}
	return ans;
}
